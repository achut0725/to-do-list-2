const name = document.getElementById("name");
const form = document.getElementById("form");
let todos = [];
function render() {
    const rootDiv = document.getElementById("root");
    rootDiv.innerHTML = "";
    for(i = 0; i < todos.length; i++){
        const newDiv = document.createElement("div");
        newDiv.classList.add("todo-item");
        const p = document.createElement("p");
        p.innerHTML = todos[i];
        newDiv.append(p);
        const button = document.createElement("button");
        button.innerHTML = "delete";
        button.id = i;
        button.addEventListener("click", (e)=>{
            console.log(button.id);
            todos = todos.filter((todo, i1)=>{
                if (i1 !== Number(button.id)) return todo;
            });
            console.log(todos);
            render();
        });
        newDiv.append(button);
        rootDiv.append(newDiv);
    }
}
form.addEventListener("submit", (e)=>{
    e.preventDefault();
    if (name.value === "") {
        console.log("empty");
        return;
    }
    todos.push(name.value);
    render();
});

//# sourceMappingURL=index.c36f364e.js.map
